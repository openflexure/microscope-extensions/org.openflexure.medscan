from labthings.server.extensions import BaseExtension
from labthings.core.utilities import path_relative_to

from openflexure_microscope.api.utilities.gui import build_gui

import logging
import json


# Read package info from package.json
with open(path_relative_to(__file__, "static", "package.json")) as f:
    package = json.load(f)
    component_name = package.get("name")

# Create our extension object
customelement_extension_v2 = BaseExtension(
    "org.openflexure.medscan",
    description=package.get("description"),
    static_folder=path_relative_to(__file__, "static", "dist")
)

# Function to return the Iframe interface
def frame_func():
    return {
        "href": customelement_extension_v2.static_file_url(""),
    }

# Build a GUI for eV (name, icon etc)
def gui_func():
    return {
        "icon": "grid_on",
        "title": "Med Scan",
        "viewPanel": "stream",
        "frame": frame_func()
    }

# Add GUI and web component to extension metadata
customelement_extension_v2.add_meta("frame", frame_func)
customelement_extension_v2.add_meta(
    "gui", build_gui(gui_func, customelement_extension_v2)
)
