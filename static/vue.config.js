process.env.VUE_APP_NAME = require('./package.json').name
module.exports = {
  publicPath: './',
  productionSourceMap: false,
  filenameHashing: false
};