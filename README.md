# [Deprecated] org.openflexure.medscan

## This extension is now deprecated (as of server v2.5.0-beta.1). To access the IHI slide scan wizard, go to the Features tab in Settings from the OpenFlexure Microscope interface.

## Installation

* `sudo -i -u openflexure-ws` (password: openflexure)
* `cd /var/openflexure/extensions/microscope_extensions`
* `git clone https://gitlab.com/openflexure/microscope-extensions/org.openflexure.medscan.git`
* `ofm restart`
